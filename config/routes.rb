Rails.application.routes.draw do
  mount Api => '/'
  mount GrapeSwaggerRails::Engine => '/api/echo/docs'
end
